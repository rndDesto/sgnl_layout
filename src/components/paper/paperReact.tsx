import React from "react";
import { SignalPaper } from "./paper";
import { createComponent } from "@lit-labs/react";

const SignalPaperReact = createComponent({
    tagName: 'signal-paper',
    elementClass: SignalPaper,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalPaperReact