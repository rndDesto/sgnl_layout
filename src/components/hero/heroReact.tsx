import React from "react";
import { SignalHero } from "./hero";
import { createComponent } from "@lit-labs/react";

const SignalHeroReact = createComponent({
    tagName: 'signal-hero',
    elementClass: SignalHero,
    react: React,
    events: {
        onClick: 'onClick',
    },
});

export default SignalHeroReact