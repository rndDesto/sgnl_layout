import { css, html, LitElement, unsafeCSS } from 'lit';
import { classMap } from 'lit-html/directives/class-map.js';
import { customElement, property } from 'lit/decorators.js';
import styles from './hero.scss?inline';

@customElement('signal-hero')
export class SignalHero extends LitElement {

    static styles = [
        css`${unsafeCSS(styles)}`
    ]
    
    @property({ type: String })
    partName?='';

    @property({ type: String })
    idName?='';

    @property({ type: String })
    testId?='';

    @property({ type: String })
    variant?: '1' | '2' = '1';

    @property({ type: String })
    classes?: string;



    render() {
        const variantProps = this.variant ?`variant-${this.variant}`:'';
        const classesProps = this.classes ?`${this.classes}`:'';

        const paperStyle = classMap({
            'tsel-hero': true,
            [variantProps]: !!this.variant,
            [classesProps]: !!this.classes,
        });

        return html`
        
        <div 
            data-testid=${this.testId === '' ?'tsel-paperid': this.testId}
            part=${this.partName === '' ?'tselpartpaper': this.partName}
            id=${this.idName === '' ?'tselidpaper': this.idName} 
            class=${paperStyle}
            >
                <slot></slot>
        </div>
        `
    }
}

declare global {
    interface HTMLElementTagNameMap {
        'signal-hero': SignalHero
    }
}