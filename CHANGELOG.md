

## 0.0.28 (2023-09-21)


### Bug Fixes

* update import css ([4ced88f](https://gitlab.com/rndDesto/sgnl_layout/commit/4ced88fd0e7d53747d166b60d9c33ce339dc6f62))

## 0.0.27 (2023-09-20)


### Bug Fixes

* edit paper ([a40be07](https://gitlab.com/rndDesto/sgnl_layout/commit/a40be07498ea66a01a90f6cf5b598076cccffd92))
* renew token ([cfe4e89](https://gitlab.com/rndDesto/sgnl_layout/commit/cfe4e89adde018796a4001cdea11e59259b06f1d))
* renew token release ([935a0c3](https://gitlab.com/rndDesto/sgnl_layout/commit/935a0c3fa7e3bd3a3f0726889ad4f1e9a78389dd))

## 0.0.24 (2023-09-01)


### Bug Fixes

* coba deply lagi ([932a71b](https://gitlab.com/rndDesto/sgnl_layout/commit/932a71bd2c7922f15c53f14110d051477e45f2ba))
* coba publish paper ([e35353b](https://gitlab.com/rndDesto/sgnl_layout/commit/e35353bddd4322ddfcb316a7ea8676d940ac401d))

## 0.0.23 (2023-09-01)


### Bug Fixes

* deploy 23 ([a0006dc](https://gitlab.com/rndDesto/sgnl_layout/commit/a0006dc63b0bc17ae683bdbc9b6203a81b3be2ac))

## 0.0.22 (2023-08-28)


### Bug Fixes

* update paper padding ([c72efab](https://gitlab.com/rndDesto/sgnl_layout/commit/c72efab054571aec8d9fc49fd976dab53fdee017))

## 0.0.21 (2023-08-28)


### Bug Fixes

* rm class clobal ([cda4af7](https://gitlab.com/rndDesto/sgnl_layout/commit/cda4af7e838fbbe048a768b8b5d1235d327dd966))
* vite config external ([b6b06be](https://gitlab.com/rndDesto/sgnl_layout/commit/b6b06be276546c1a28e53521a575d5e54b00c3fb))

## 0.0.20 (2023-08-22)


### Bug Fixes

* ganti nama paket ([e3841b6](https://gitlab.com/rndDesto/sgnl_layout/commit/e3841b6f519be8f4d8806ebf9181d183d69c7dbf))

## 0.0.19 (2023-08-21)


### Bug Fixes

* ganti key ([01d576c](https://gitlab.com/rndDesto/sgnl_layout/commit/01d576cfc0acb4c3c7ee73e79bfc7810e0eb63be))
* ganti warna ([6c2b38c](https://gitlab.com/rndDesto/sgnl_layout/commit/6c2b38cee7058baecf36d238f0f910cc11ce10f3))

## 0.0.18 (2023-08-21)


### Bug Fixes

* ganti deploy registy ke sngl ([406bc3f](https://gitlab.com/rndDesto/sgnl_layout/commit/406bc3fb7310b108384720f6a02e2177e3471be5))

## 0.0.17 (2023-08-18)


### Bug Fixes

* init layout ([4746f45](https://gitlab.com/rndDesto/sgnl_layout/commit/4746f458396590cbf84fafac5fd45f132f6e5760))

## 0.0.16 (2023-08-18)


### Bug Fixes

* enhance folder ([c4a92dd](https://gitlab.com/rndDesto/sgnl_typography/commit/c4a92dd9ed70dfbb0bea69ae2aa36f22185a79fb))

## 0.0.15 (2023-08-14)


### Bug Fixes

* color title ([2672ae0](https://gitlab.com/rndDesto/sgnl_typography/commit/2672ae033ed6941762562add213974c65e78ce2a))

## 0.0.14 (2023-08-14)


### Bug Fixes

* coba publish lagi ([c64db46](https://gitlab.com/rndDesto/sgnl_typography/commit/c64db460dca6f6f658ba4784fcb40ea3b30ccfef))

## 0.0.13 (2023-08-14)


### Bug Fixes

* tambah script before ([6b63519](https://gitlab.com/rndDesto/sgnl_typography/commit/6b6351918000e56ed25b4efd7bb2cd7358a5317e))

## 0.0.12 (2023-08-14)


### Bug Fixes

* fixing body ([400258d](https://gitlab.com/rndDesto/sgnl_typography/commit/400258d6e6f167b44dbcf49b01976d251508c21d))

## 0.0.11 (2023-08-14)


### Bug Fixes

* edit signal-body ([cc34066](https://gitlab.com/rndDesto/sgnl_typography/commit/cc340660a0050b6f01357a496f1a7fb499e7b082))

## 0.0.10 (2023-08-14)


### Bug Fixes

* cb css ([facbdcd](https://gitlab.com/rndDesto/sgnl_typography/commit/facbdcd17089aa9651e496f3c78cea1720c1fd7e))

## 0.0.9 (2023-08-14)


### Bug Fixes

* coba css dari luar ([6f87936](https://gitlab.com/rndDesto/sgnl_typography/commit/6f879367e7ec57b83576414a1adde70a606d2097))
* coba css dari luar ([691bfae](https://gitlab.com/rndDesto/sgnl_typography/commit/691bfae212ce0fa1bca0155923ca493e032d1d92))
* **coba css dari luar:** coba apply css dari existing class ([303e21b](https://gitlab.com/rndDesto/sgnl_typography/commit/303e21b7378393f4f98acf7fa89584fcb66f9c51))
* coba style dari luar ([06313e8](https://gitlab.com/rndDesto/sgnl_typography/commit/06313e88cfc56b8f2b9d107a2586b25c60300955))

## 0.0.8 (2023-08-11)


### Bug Fixes

* coba republish ([9718f10](https://gitlab.com/rndDesto/sgnl_typography/commit/9718f10d6a8b1191ac998d658f106a95d2fed384))
* publihs lagi ([8c8edb9](https://gitlab.com/rndDesto/sgnl_typography/commit/8c8edb9b057536f60e9a2ca2a33fedf2ae1a980c))

## 0.0.7 (2023-08-11)


### Bug Fixes

* apus config paket ([1c21e46](https://gitlab.com/rndDesto/sgnl_typography/commit/1c21e462efd6cc6a6cf9c5029d29637c7eac0bc1))

## 0.0.6 (2023-08-11)


### Bug Fixes

* rebuild lagi ([5413279](https://gitlab.com/rndDesto/sgnl_typography/commit/5413279860e43ea119c3025ea7eba7a47abdc568))

## 0.0.5 (2023-08-11)


### Bug Fixes

* tambah heading ([699e4ef](https://gitlab.com/rndDesto/sgnl_typography/commit/699e4ef6cf875e474df29af30dbd639e15db3445))

## 0.0.4 (2023-08-10)


### Bug Fixes

* add native style ([000edc2](https://gitlab.com/rndDesto/sgnl_typography/commit/000edc2548dc153389a362a1dde692ef3ace3665))

## 0.0.3 (2023-08-10)


### Bug Fixes

* better folder ([d6f1800](https://gitlab.com/rndDesto/sgnl_typography/commit/d6f1800d3f0971b8cb7eb5bf5cbdfc6318868cc1))

## 0.0.2 (2023-08-10)


### Bug Fixes

* default bundle ([0a4eed4](https://gitlab.com/rndDesto/sgnl_typography/commit/0a4eed45e7a5332d44154e90571ebcc99b6203c1))
* fixing publish ([345e8e0](https://gitlab.com/rndDesto/sgnl_typography/commit/345e8e07062c98a41a7aabaa9a2e79f88cdb4115))

## 0.0.1 (2023-08-10)


### Bug Fixes

* **init commit:** inisial commit ([670cac2](https://gitlab.com/rndDesto/sgnl_typography/commit/670cac2a4f6376bd4fa9c6ac78ced2e33b6aeafc))